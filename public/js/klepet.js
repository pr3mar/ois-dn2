var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.kanalZGeslom = function(kanal, geslo) {
  this.socket.emit('pridruzitevGesloZahteva', {
    novKanal: kanal,
    geslo: geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      besede = besede.join(' ');
      //console.log(besede);
      //console.log(besede.split('"'));
      if(besede.split('"').length < 4) {
        console.log("brez gesla");
        this.spremeniKanal(besede);
      } else {
        //console.log("geslo");
        besede = besede.split('"');
        besede.shift();
        var kanal = besede.shift();
        besede.shift();
        besede.pop();
        var geslo = besede.join('"');
        this.kanalZGeslom(kanal, geslo);
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede = besede.join(' ');
      besede = besede.split('"');
      if (besede.length < 4) {
        sporocilo = 'Neznan ukaz.';
        break;
      }
      //console.log(besede);
      besede.shift();
      vzdevek = besede.shift();
      
      //console.log(besede);
      besede.shift();
      besede.pop();
      var sporociloTekst = besede.join('"');
      //sporociloTekst = sporociloTekst.replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
      //console.log(vzdevek);
      //console.log(sporociloTekst);
      this.socket.emit('zasebno', {
          vzdevek: vzdevek, 
          sporocilo: sporociloTekst
      });
      //sporocilo = "(zasebno za <b>".concat(vzdevek, "</b>): ", sporociloTekst);
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };
  //return sporocilo.replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
  return sporocilo;
};