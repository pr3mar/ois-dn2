var swearWords = [];

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function divElementHtmlTekstSmesek(sporocilo) {
  return $('<div></div>').html('<b>' + sporocilo + '</b>');
}

String.prototype.replaceAll = function(str1, str2, ignore) 
{
    return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
} 

/* naloga 2.1 Podpora uporabi smeškov */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
  //var pattern = ":\)|;|\(y\)|:\*|:\(";
  
  sporocilo = sporocilo.replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;");
  sporocilo = setEmotions(sporocilo);
  $.get('/swearWords.csv', function(data) {
    var swearWords = data.split(',');
    for(var i = 0; i < swearWords.length; i++) {
      var subs = "";
      for(var j = 0; j < swearWords[i].length; j++)
        subs = subs.concat("*");
      var t = "";
      t = t.concat("\\b",swearWords[i],"\\b");
      //console.log(t);
      var reg = new RegExp(t, "gi");
      sporocilo = sporocilo.replace(reg, subs);
    }
    if (sporocilo.charAt(0) == '/') {
      sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
      if (sistemskoSporocilo) {
        $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      }
    } else {
      var kanal = $('#kanal').text();
      kanal = kanal.substring(kanal.indexOf("@") + 2, kanal.length);
      //console.log(kanal);
      klepetApp.posljiSporocilo(kanal, sporocilo);
      //$('#sporocila').append(divElementEnostavniTekst(sporocilo));
      $('#sporocila').append(divElementHtmlTekstSmesek(sporocilo));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    }
    
  });
  $('#poslji-sporocilo').val('');
}

function setEmotions(sporocilo) {
  var emotions = {
    ";)" : "<img src=\'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png\' alt=\';)\'>",
    ":)" : "<img src=\'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png\' alt=\':)\'>",
    "(y)" : "<img src=\'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png\' alt=\'(y)\'>",
    "(Y)" : "<img src=\'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png\' alt=\'(Y)\'>",
    ":*" : "<img src=\'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png\' alt=\':*\'>",
    ":(" : "<img src=\'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png\' alt=\':(\'>"
  };
  
  for (var tmp in emotions){
    if (emotions.hasOwnProperty(tmp)) {
      sporocilo = sporocilo.replaceAll(tmp, emotions[tmp]);
    }
  }
  //console.log(sporocilo);
  return sporocilo;
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      var cont = $('#kanal').text();
      var index = cont.indexOf('@');
      cont = cont.substring(index +  2, cont.length);
      $('#kanal').text(rezultat.vzdevek + " @ " + cont);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    if(rezultat.uspesno){
      $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal);
      $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    } else {
      $('#sporocila').append(divElementHtmlTekst(rezultat.besedilo));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    }
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });
  
  // zasebni sporocili error
  socket.on('zasebno', function (zasebno) {
    //var novElement = $('<div style="font-weight: bold"></div>').text(zasebno.sporocilo);
    $('#sporocila').append(divElementHtmlTekst(zasebno.sporocilo));
  });
  
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(rezultat) {
    //console.log(rezultat.uporabniki);
    $('#seznam-uporabnikov').empty();
    
    for(var i = 0; i < rezultat.uporabniki.length; i++) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(rezultat.uporabniki[i]));
    }
    $('#seznam-uporabnikov div').click(function() {
      var text = '/zasebno "' + $(this).text() + '" "';
      console.log(text);
      $('#poslji-sporocilo').val(text);
      $('#poslji-sporocilo').focus();
    });      
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  // novo
  setInterval(function() {
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
  
});