var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var gesloKanala = {}; // kanal: "geslo"

/* naloga 2.5 - seznam uporabnikov */

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajPosredovanjeZasebnegaSporocila(io, socket, vzdevkiGledeNaSocket, uporabljeniVzdevki)
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajPridruzitevKanaluZGeslom(socket);
    socket.on('kanali', function() {
      for(var el in gesloKanala) {
        if(!(('/' + el) in io.sockets.manager.rooms)) {
          delete gesloKanala[el];
        }
      }
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    // novo
   socket.on('uporabniki', function() {
      //console.log(trenutniKanal[socket.id], vzdevkiGledeNaSocket[socket.id]);
      var uporabnikiNaKanalu = io.sockets.clients(trenutniKanal[socket.id]);
      //console.log(uporabnikiNaKanalu);
      var uporabniki = [], j = 0;
      
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        if(uporabnikSocketId == socket.id){
          uporabniki.push(vzdevkiGledeNaSocket[uporabnikSocketId] + "(jaz)");
        } else {
          uporabniki.push(vzdevkiGledeNaSocket[uporabnikSocketId]);
        }
        j++;
      }
      if (uporabniki.length > 0) {
        socket.emit('uporabniki', {
          uporabniki:uporabniki
        });
      }
    });
    
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek,
    kanal: trenutniKanal[socket.id]
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  socket.emit('pridruzitevOdgovor', {
    kanal: kanal, 
    vzdevek: vzdevkiGledeNaSocket[socket.id],
    uspesno: true
  });
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function pridruzitevKanaluZGeslom(socket, kanal, geslo) {
  var kanali = io.sockets.manager.rooms;
  var obstaja = false;
  var iskani = false;
  
  for(var kan in kanali) {
    kan = kan.substring(1, kan.length);
    if (kan == kanal) {
      obstaja = true;
      iskani = kan;
      break;
    }
  }
  var gesloKanal  = '';
  
  if(obstaja && (iskani in gesloKanala)){
    //console.log('1.');
    gesloKanal = gesloKanala[iskani];
  } else if (obstaja && !(iskani in gesloKanala)){
    // ni password protected
    //console.log('2.');
    socket.emit('pridruzitevOdgovor', {
        uspesno: false,
        kanal: kanal,
        besedilo: "Izbrani kanal <b>" + kanal + "</b> je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite " +
        "z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom."
      })
    return;
  } else {
    //console.log('3.');
    gesloKanala[kanal] = geslo;
    gesloKanal = geslo;
  }
  //console.log(gesloKanala)
  if(gesloKanal !== '' && geslo == gesloKanal){
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, kanal);
  } else {
    socket.emit('pridruzitevOdgovor', {
      uspesno: false,
      kanal: kanal,
      besedilo: "Pridružitev v kanal <b>" + kanal + "</b> ni bilo uspešno, ker je geslo napačno!"
    });
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPosredovanjeZasebnegaSporocila(io, socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('zasebno', function (zasebno) {
    //console.log(zasebno);
    if(uporabljeniVzdevki.indexOf(zasebno.vzdevek) != -1) {
      var userSocket = false;
      //console.log(vzdevkiGledeNaSocket);
      for( var each in vzdevkiGledeNaSocket) {
        if( vzdevkiGledeNaSocket.hasOwnProperty( each ) ) {
             if( vzdevkiGledeNaSocket[ each ] == zasebno.vzdevek ){
                 userSocket =  each;
                 break;
             }
        }
      }
      if(userSocket == socket.id){
        var nazaj = "Sporočilo '".concat(zasebno.sporocilo, "' uporabniku z vzdevkom '", zasebno.vzdevek,"' ni bilo mogoče posredovati.");
        socket.emit('zasebno', {
          sporocilo:nazaj
        });
      } else {
        io.sockets.sockets[userSocket].emit('sporocilo', {
          besedilo: vzdevkiGledeNaSocket[socket.id] + " (zasebno): " +  zasebno.sporocilo
        });
        socket.emit('sporocilo', {
          besedilo: "(Zasebno za " + zasebno.vzdevek + "): " + zasebno.sporocilo
        })
      }
    } else {
      var nazaj = "Sporočilo '".concat(zasebno.sporocilo, "' uporabniku z vzdevkom '", zasebno.vzdevek,"' ni bilo mogoče posredovati.");
      socket.emit('zasebno', {
        sporocilo:nazaj
      });
    }
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    //console.log(gesloKanala);
    if(kanal.novKanal in gesloKanala){
      socket.emit('pridruzitevOdgovor', {
        uspesno: false,
        kanal: kanal.novKanal,
        besedilo: "Kanal <b>" + kanal.novKanal + "</b> je zasciten z geslom."
      })
    } else {
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal);
    }
  });
}

function obdelajPridruzitevKanaluZGeslom(socket) {
  socket.on('pridruzitevGesloZahteva', function(kanal) {
    //socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanaluZGeslom(socket, kanal.novKanal, kanal.geslo);
  });
}


function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}